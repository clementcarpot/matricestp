﻿using System;

//https://stackoverflow.com/questions/4260207/how-do-you-get-the-width-and-height-of-a-multi-dimensional-array

namespace Matrix
{
    class SquareMatrix
    {
        private int [,] sqmatrix;
        private int size;

        // Le nom de la fonction SquareMatrix le type de retour est un int
        // sqmatrix est un int
        // La fonction recupère la taille définie par sqmatrix
        public SquareMatrix (int [,] sqmatrix) {
            this.sqmatrix = sqmatrix;
            //this.size = sqmatrix.Length;
            this.size = sqmatrix.GetLength(0);
        }

        // La fonction SquareMatrice crée la matrice
        // size est un int defaultValue est aussi un int
        // sqmatrix est un int
        public SquareMatrix (int size, int defaultValue) {
            this.sqmatrix = new int [size, size];
            this.size = size;
            for (int i = 0; i < size; i++) {
                for (int j = 0; j < size; j++){
                    this.sqmatrix[i, j] = defaultValue;
                }
            }
        }

        // La fonction s'appelle GetSize 
        // On retourne size la taille de la matrice
        // size est un int 
        
        public int GetSize () {
            return this.size;
        }
        
        // La fonction s'appelle ToString
        // Le retour affiche une chaîne de caractère
        // ret est  un string size est un int i et j sont un int et smatrix est int(tableau)
        // La fonction crée une matrice 2x2 
        public string ToString() {
            string ret = "";
            for (int i = 0; i < this.size; i++) {
                //Console.Write("| ");
                ret = ret + "| ";
                for (int j = 0; j < this.size; j++) {
                    //Console.Write(test[i,j] + " | ");
                    ret = ret + this.sqmatrix[i,j] + " | ";
                }
                //Console.WriteLine();
                ret = ret + "\n";
            }
            return ret;
        }

        // La fonction s'appelle ToString2
        // Le retour affiche une chaîne de caractère
        // ret est  un string size est un int i et j sont un int et smatrix est int(tableau)
        // La fonction crée une matrice 2x2 
        public string ToString2() {
            string ret = "";
            for (int i = 0; i < this.size; i++) {
                //Console.Write("| ");
                ret = ret + " ";
                for (int j = 0; j < this.size; j++) {
                    //Console.Write(test[i,j] + " | ");
                    ret = ret + this.sqmatrix[i,j] + " ";
                }
                //Console.WriteLine();
                ret = ret + "\n";
            }
            return ret;
        }
        
        // La fonction s'appelle HoriLine
        // x et j sont des int newvalue est un int size est aussi un int
        // La fonction a pour but d'écrire une ligne horizontale dans la matrice avec une valeur prédéfinie dans le fichier test
        public void HoriLine(int x, int newValue) {
            if (x >= 0 && x < this.size) {
                for (int j = 0; j < this.size; j++) {
                    this.sqmatrix[x, j] = newValue;
                }
            }
        }

        // La fonction s'appelle HoriSegment
        // x et j sont des int value est un int size est aussi un int y0 et y1 sont des int
        // La fonction a pour but d'écrire un segment dans la matrice avec une valeur prédéfinie dans le fichier test
        public void HoriSegment(int x, int value, int y0, int y1) {
            if (x >= 0 && x < this.size &&
                y0 >= 0 && y0 < this.size &&
                y1 >= 0 && y1 < this.size 
               ) 
               {
                   for (int j = y0 ; j <= y1; j ++) {
                       this.sqmatrix[x, j] = value;
                   }
               }
        }

        // La fonction s'appelle HoriSegmentL
        // x et j sont des int value est un int size est aussi un int longueur est un int
        // La fonction a pour but d'écrire un segment horizontale dans la matrice avec une valeur prédéfinie dans le fichier test
        public void HoriSegmentL(int x, int value, int y0, int longueur) {
            int y1 =  y0 + longueur;
            if (x >= 0 && x < this.size &&
                y0 >= 0 && y0 < this.size &&
                y1 >= 0 && y1 < this.size 
               ) 
               {
                   for (int j = y0 ; j <= y1; j ++) {
                       this.sqmatrix[x, j] = value;
                   }
               }
        }

        // La fonction s'appelle Rectangle
        // x0 et y0 sont des int value est un int size est aussi un int longueur et hauteur sont des int
        // La fonction a pour but d'écrire un rectangle dans la matrice avec une valeur prédéfinie dans le fichier test
        public void Rectangle (int hauteur, int longueur, int x0, int y0, int value) {
            if (x0 >= 0 && x0 + longueur < this.size &&
                y0 >= 0 && y0 + hauteur < this.size
               )
               {
                   for (int i = x0; i < x0 + hauteur; i++) {                       
                           this.HoriSegmentL(i, value, y0, longueur-1);
                   } 
               }
        }

        // La fonction s'appelle VertiLine
        // y et i sont des int newValue est un int size est aussi un int sqmatrix est un int
        // La fonction a pour but d'écrire une ligne verticale dans la matrice avec une valeur prédéfinie dans le fichier test
        public void VertiLine(int y, int newValue) {
            if (y >= 0 && y < this.size) {
                for (int i = 0; i < this.size; i++) {
                    this.sqmatrix[i, y] = newValue;
                }
            }
        }

        // La fonction s'appelle Line
        // x0, x1, y0, y1 sont des int value est un int coeffdir est aussi un int
        // La fonction a pour but d'écrire une ligne diagonale dans la matrice avec une valeur prédéfinie dans le fichier test
        public void Line(int x0, int y0, int x1, int y1, int value) {
            int coeffdir = (y1-y0) / (x1-x0);
            Console.WriteLine(coeffdir);
            int constant = y0 - coeffdir*x0;
            Console.WriteLine(constant);

            int y;
            for (int x = x0; x <= x1; x++) {
                y = x * coeffdir + constant;
                this.sqmatrix[x, y]  = value;
            }
        }

        // La fonction s'appelle Set
        // x, y, val et sqmatrix sont des int
        // Elle définie la taille de la matrice
        public void Set(int x, int y, int val) {
            this.sqmatrix[x,y] = val;
        }

        // La fonction s'appelle test
        // elle retourne une chaine de caractère
        public static string test () {
            return "La vie c'est beau!";
        }

      
        /*public void ToPBMFile(string file) {
            string [] lines = new  string [2 + this.sqmatrix.Length];
            lines[0] = "P1";
            lines[1] = this.size.ToString() + " " + this.size.ToString();
            for (int i = 0; i < this.size; i++) {
                for (int j = 0; j < this.size; j++) {
                    lines[this.size *i+j + 2] = this.sqmatrix[i, j].ToString();
                }
            }
            System.IO.File.WriteAllLines(@file, lines);
        }
        
    }
}
