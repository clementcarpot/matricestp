using System;

namespace Matrix
{
    class TestSquareMatrix
    {
        static void Main(string[] args)
        {

            SquareMatrix sq4 = new SquareMatrix(4, 42);

            SquareMatrix sq5 = new SquareMatrix(30, 0);

            sq5.HoriLine(12, 1);

            sq5.VertiLine(12, 2);

            int x0 = 3;
            int y0 = 6;
            int x1 = 17;
            int y1 = 26;

            sq5.Line(x0, y0, x1, y1, 3);

            sq5.HoriSegment(17, 4, 12, 29);

            sq5.HoriSegmentL(22, 5, 17, 11);

            sq5.Rectangle(5, 9, 9, 9, 8);
            Console.WriteLine(sq5.ToString2());

            SquareMatrix sq6 = new SquareMatrix(300, 1);
            sq6.Rectangle(20, 150, 100, 200, 0);
            //sq6.ToPBMFile("test.pbm");
        }
    }
}