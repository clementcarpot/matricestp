==============
Documentation 
==============
____________________________________________

Partie 1 : Comprendre ce qu'est un tableau
____________________________________________

Un tableau est une structure de données de taille fixe représentant une séquence d'éléments que l'on peut parcourir grâce a leur position dans la séquence. C'est un type de conteneur que l'on retrouve dans un grand nombre de langage de programmation.
Un tableau peut contenir des valeurs de tout types (entier, chaine de caractère, booleen, etc...)

Les tableaux permettent d'accéder immédiatement à un élément qui le compose

Pour déclarer un tableau en algo on fait comme ceci :
	
	nomDuTableau[valeurDebut..valeurFin] ou nomDuTableau[dimensionTableau]

Ensuite pour accéder à un élément du tableau on procéde de la manière suivante :

	tab[5];		# Ici on retournera l'élément du tableau qui a pour index 5, soit la 				6ème valeur car la première valeur a pour indice 0.

Pour calculer la longueur d'un tableau on peut utiliser la variable length.

____________________________________________

Partie 2 : Comprendre ce qu'est une matrice
____________________________________________

Une matrice est un tableau a deux dimensions là ou le tableau ne comporte qu'une dimension.
La matrice peut être considéré comme un regroupement de plusieurs tableaux de même taille.

Pour déclarer une matrice on procède ainsi :

	nomMatrice : Tableau de Nombre-lignes * Nombre -colonnes type-éléments

De même que pour le tableau, les lignes et colonnes d'une matrice sont numérotés par des indices.


__________________________________________

Partie 3 : Comprendre ce qu'est une liste
__________________________________________

Une liste est comme un tableau mais il n'est pas nécessaire de déclarer sa taille avant de l'utiliser, sa taille s'adaptera automatiquement lorsque nous lui ajouterons/supprimerons des éléments.
Une liste ne peut contenir que des objets et nous permet d'utiliser des types génériques pour assurer la sécurité des types.

Pour calculer la taille d'une liste on utilise la méthode size().

____________________________________________________________

Partie 4 : Pourquoi implémenter une matrice avec des listes
____________________________________________________________


L'intérêt de cette nouvelle implémentation qui utilise les listes est de pouvoir rajouter des valeurs à notre matrice là où nous aurions été limité par les tableaux et leurs tailles fixe.







