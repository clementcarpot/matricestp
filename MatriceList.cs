using System;

class MatriceList
{
    static void Display(int[,] array)
    {
        // Loop over 2D int array and display it.
        for (int i = 0; i <= array.GetUpperBound(0); i++)
        {
            for (int x = 0; x <= array.GetUpperBound(1); x++)
            {
                Console.Write(array[i, x]);
                Console.Write(" ");
            }
            Console.WriteLine();
        }
    }

    static void Main()
    {
        
        int[,] values = { { 0,0,0,0,0,0,0,0,0 }
                        , { 0,0,0,0,0,0,0,0,0 }
                        , { 0,0,0,0,0,0,0,0,0 }
                        , { 0,0,0,0,0,0,0,0,0 }};
        Console.WriteLine("");
        Display(values);
       
    }
}